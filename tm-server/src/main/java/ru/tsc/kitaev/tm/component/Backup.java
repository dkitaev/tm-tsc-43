package ru.tsc.kitaev.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kitaev.tm.api.service.IDomainService;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Backup {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final IDomainService domainService;

    private static final int INTERVAL = 30;

    public Backup(@NotNull final IDomainService domainService) {
        this.domainService = domainService;
    }

    public void init() {
        load();
        es.scheduleWithFixedDelay(this::save, 0, INTERVAL, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    public void save() {
        domainService.dataBackupSave();
    }

    public void load() {
        domainService.dataBackupLoad();
    }

}
